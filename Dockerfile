FROM python:3

ENV PYTHONUNBUFFERED 1

WORKDIR /src
COPY . /src/
RUN pip install --upgrade pip && pip install -U -r requirements.txt
