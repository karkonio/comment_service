from django.contrib import admin
from polymorphic.admin import (
    PolymorphicParentModelAdmin,
    PolymorphicChildModelAdmin,
    PolymorphicChildModelFilter,
)

from .models import UserProfile, Post, Comment, ChildComment, PostComment


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', )
    readonly_fields = ('username', 'first_name', 'last_name', 'email', )

    def username(self, obj):
        return obj.user.username

    def first_name(self, obj):
        return obj.user.first_name

    def last_name(self, obj):
        return obj.user.last_name

    def email(self, obj):
        return obj.user.email


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('author', 'title', 'comment_count', )
    fields = ('author', 'title', 'text', )

    def comment_count(self, obj):
        return obj.comment_count


class CommentChildAdmin(PolymorphicChildModelAdmin):
    base_model = Comment


@admin.register(ChildComment)
class ChildCommentAdmin(CommentChildAdmin):
    base_model = ChildComment
    fields = ('parent', "author", "text", )


@admin.register(PostComment)
class PostCommentAdmin(CommentChildAdmin):
    base_model = PostComment
    fields = ('post', "author", "text", )


@admin.register(Comment)
class CommentParentAdmin(PolymorphicParentModelAdmin):
    list_display = ("id", "author", "text",)
    list_filter = (PolymorphicChildModelFilter, )
    base_model = Comment
    child_models = (ChildComment, PostComment, )
