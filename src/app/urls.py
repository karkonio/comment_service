from django.urls import path

from .views import CommentViewSet, PostViewSet

urlpatterns = [
    path("comments/", CommentViewSet.as_view({"get": "list", "post": "create"})),
    path("comments/<int:pk>/", CommentViewSet.as_view({
        "get": "retrieve", "delete": "destroy", "patch": "partial_update"
    })),
    path("posts/", PostViewSet.as_view({"get": "list"})),
    path("posts/<int:pk>/", PostViewSet.as_view({"get": "retrieve"})),

    # path("vehicles/<uuid:vehicle>/", VehicleView.as_view({"get": "retrieve", "delete": "destroy"})),
    # path("shifts/", ShiftView.as_view({"get": "list"})),
    # path("shifts/<uuid:shift>/", ShiftView.as_view({"get": "retrieve"})),
]
