from django.db import models
from django.contrib.auth.models import User
from polymorphic.models import PolymorphicModel

from core.models import TimestampModel


class UserProfile(models.Model):
    user = models.OneToOneField(
        to='auth.User',
        on_delete=models.CASCADE,
        related_name="profile"
    )

    def __str__(self) -> str:
        return f"Profile user={self.user.username} id={self.id}"


class Post(TimestampModel, models.Model):
    author = models.ForeignKey(
        to='UserProfile',
        on_delete=models.CASCADE,
        related_name='posts'
    )
    title = models.CharField(max_length=100)
    text = models.TextField()

    def __str__(self) -> str:
        return f"Post author={self.author.user.username} id={self.id}"

    @property
    def comment_count(self):
        child_comments_count = []
        for comment in self.comments.all():
            child_comments_count.append(
                sum([len(c.get_all_children()) for c in ChildComment.objects.filter(parent=comment)])
            )
        return sum(child_comments_count) + self.comments.all().count()


class Comment(PolymorphicModel, TimestampModel):
    author = models.ForeignKey(
        to='UserProfile',
        on_delete=models.CASCADE,
        related_name='comments'
    )
    text = models.TextField()

    def __str__(self) -> str:
        return str(self.text)

    @property
    def has_child(self):
        return ChildComment.objects.filter(parent=self).exists()

class ChildComment(Comment):
    parent = models.ForeignKey(
        to='Comment',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='children'
    )

    def get_all_children(self, include_self=True):
        r = []
        if include_self:
            r.append(self)
        for c in ChildComment.objects.filter(parent=self):
            _r = c.get_all_children(include_self=True)
            if 0 < len(_r):
                r.extend(_r)
        return r


class PostComment(Comment):
    post = models.ForeignKey(
        to='Post',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='comments'
    )
