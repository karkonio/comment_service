from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from .models import Comment, ChildComment, PostComment, Post


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ["id", "author", "text"]


class ChildCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChildComment
        fields = ["id", "author", "text", "parent"]


class PostCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostComment
        fields = ["id", "author", "text", "post"]


class CommentPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Comment: CommentSerializer,
        ChildComment: ChildCommentSerializer,
        PostComment: PostCommentSerializer
    }


class ChildCommentDetailedSerializer(serializers.ModelSerializer):
    children = serializers.SerializerMethodField(read_only=True)

    def get_children(self, obj):
        qs = ChildComment.objects.filter(parent=obj)
        serializer = ChildCommentDetailedSerializer(instance=qs, many=True)
        return serializer.data

    class Meta:
        model = ChildComment
        fields = ["id", "author", "text", "parent", "children"]


class CommentDetailSerializer(serializers.ModelSerializer):
    children = serializers.SerializerMethodField(read_only=True)

    def get_children(self, obj):
        qs = ChildComment.objects.filter(parent=obj)
        serializer = ChildCommentDetailedSerializer(instance=qs, many=True)
        return serializer.data

    class Meta:
        model = Comment
        fields = ("id", "created_at", "author", "text", "children", )


class CommentUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ("id", "created_at", "author", "text", )


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ("id", "author", "title", "text", "comment_count", )


class PostDetailSerializer(serializers.ModelSerializer):
    comments = serializers.SerializerMethodField(read_only=True)

    def get_comments(self, obj):
        qs = PostComment.objects.filter(post=obj)
        serializer = CommentDetailSerializer(instance=qs, many=True)
        return serializer.data

    class Meta:
        model = Post
        fields = (
            "id",
            "created_at",
            "author",
            "title",
            "text",
            "comment_count",
            "comments",
        )
