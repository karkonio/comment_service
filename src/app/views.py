from rest_framework import viewsets, status
from rest_framework.response import Response
from django_filters import rest_framework as filters

from .models import Comment, Post, UserProfile

from .serializers import (
    CommentPolymorphicSerializer, CommentDetailSerializer,
    PostSerializer, PostDetailSerializer,
    CommentUpdateSerializer,
)


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentPolymorphicSerializer

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return CommentDetailSerializer
        if self.action == 'delete':
            return CommentDetailSerializer
        return self.serializer_class

    def partial_update(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            serializer = CommentUpdateSerializer(
                instance, data=request.data, partial=True
            )
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        except status.HTTP_404_NOT_FOUND:
            return Response(status=status.HTTP_404_NOT_FOUND)


    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            if not instance.has_child:
                self.perform_destroy(instance)
                return Response(status=status.HTTP_204_NO_CONTENT)
            return Response(
                data='Comment has subcomments, cant delete',
                status=status.HTTP_412_PRECONDITION_FAILED
            )
        except status.HTTP_404_NOT_FOUND:
            return Response(status=status.HTTP_404_NOT_FOUND)


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return PostDetailSerializer
        return self.serializer_class
