from django.db import models
from django.utils import timezone


class TimestampModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    updated_at = models.DateTimeField(blank=True, null=True,  db_index=True)

    class Meta:
        abstract = True

    def __str__(self) -> str:
        return str(self.created_at)

    def save(self, *args, **kwargs):
        if self.pk:
            self.updated_at = timezone.now()
        return super().save(*args, **kwargs)

    @property
    def changed(self):
        return bool(self.updated_at)
